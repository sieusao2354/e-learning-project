import { combineReducers } from "redux";
import { courseReducer } from "./courseReducer";
import { userReducer } from "./userReducer";


export let rootReducers = combineReducers({
    courseReducer, userReducer
})